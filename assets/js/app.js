
const vm = new Vue({
    el: '#appt',
    data: {
        results: [],
        timer: ''
    },
    methods: {
        fetchDatafromsheet: function() {
            var publicSpreadsheetUrl = 'https://docs.google.com/spreadsheets/d/1KcfDezVOTQwZlKo-ItNuOYvMm6xuy5ha3aCb7FC5mkY/pubhtml';
            Tabletop.init({
                key: publicSpreadsheetUrl,
                callback: this.showInfo,
                simpleSheet: true
            })
        },
        showInfo: function(data, tabletop) {
            this.results = data
        }
    },
    created: function() {              
        this.fetchDatafromsheet()
        this.timer = setInterval(this.fetchDatafromsheet, 10000)
    },
    beforeDestroy() {
        clearInterval(this.timer)
    }
});